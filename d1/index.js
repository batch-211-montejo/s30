/*
AGGREGATION
-Definitopn

AGGREGATION METHODS:
1. match
2. Group
3. Sort
4. Project

AGGREGATION PIPELINES

OPERATORS:
1. Sum
2. Max
3. Min
4. Avg

*/

// MONGODB AGGREGATION
/*
	-used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
	-comared to doing CRUD Operations on our data from our previous sessions, aggresgation gives us access to manipulate, filter, and computer for results, providing us iwth info to make necessary development decisions.
	-aggregations in MongoDB are very felexibile and you can form your own aggregation pipeline depending on the need ot your application
*/


// AGGREGATE METHODS
/*
#1 MATCH: "$match"
	-the "$match" is used to pass the docs that meet teh specified condition(s) to the next pipeline stage/aggregations process
	-Syntax:
		{ $match: {field:value}}

*/
	db.fruits.aggregate([
			{$match: {onSale: true}}
		])

	db.fruits.aggregate([
			{$match: {stock: {$gte:20}}}
		])


/*
#2 GROUP: "$group"
	-the "$group" is used to group elements together field-value pairs using the data from the grouped elemet.
	-Syntax:
		{group: {_id: "value", fieldResult: "valueResult"}}
*/

	db.fruits.aggregate([
			{$group: {_id: "#supplier_id", total: {$sum: "$stock"}}}
		]);


/*
#3 SORT: "$sort"
	-the "$sort" can be used when changing the order of aggregated results
	-providing a value of -1 will sort the docs in a descending order
	-providing a value of 1 will sort the docs in an ascending order
	-Syntax:
		{$sort {field: 1/-1}}
*/
	db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{ $sort: {total: -1} }
]);


/*
#4 PROJECT: $project
	-the "$project" can be used when aggregating data to include or exclude fields from the returned results
	-Syntax:
		{$project: {field: 1/0}}
*/
	db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id: 0}}
		])

	db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id: 0}},
			{$sort: {total:-1}}
		]);


/*
AGGREGATE PIPELINES
	-the aggregation pipeline in MongoDB is a framework for data aggregation
	-each stage transforms the docs as they pass through the deadlines
	-Syntax:
		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}	
		])
*/


// OPERATORS

// #1 "$sum" - gets the total of everything
	db.fruits.aggregate([
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	])

// #2 "$max" - gets the highest value out of everything else
	db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	])

// #3 "$min" - gets the lowest value out of everything
	db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	])

// #4 "$avg" - gets average value of all the fields
	db.fruits.aggregate([
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
	])